import Vue from "vue";
import Router from "vue-router";
import Login from "./views/Login.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "login",
      component: Login
    },
    {
      path: "/food-tracker",
      name: "food-tracker",
      component: () => import("./views/FoodTracker.vue")
    },
    {
      path: "/register",
      name: "register",
      component: () => import("./views/Register.vue")
    }
  ]
});
