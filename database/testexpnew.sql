--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: calories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.calories (
    id integer NOT NULL,
    calories integer NOT NULL,
    entry_date date
);


ALTER TABLE public.calories OWNER TO postgres;

--
-- Name: calories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.calories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calories_id_seq OWNER TO postgres;

--
-- Name: calories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.calories_id_seq OWNED BY public.calories.id;


--
-- Name: something; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.something (
    id integer NOT NULL
);


ALTER TABLE public.something OWNER TO postgres;

--
-- Name: calories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calories ALTER COLUMN id SET DEFAULT nextval('public.calories_id_seq'::regclass);


--
-- Data for Name: calories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.calories (id, calories, entry_date) FROM stdin;
3	120	2019-01-03
4	2	2019-01-01
\.


--
-- Data for Name: something; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.something (id) FROM stdin;
2
\.


--
-- Name: calories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.calories_id_seq', 4, true);


--
-- PostgreSQL database dump complete
--

