var pg = require('pg')
var format = require('pg-format')
var PGUSER = 'postgres'
var PGDATABASE = 'test'

var config = {
    user: PGUSER,
    database: PGDATABASE,
    max: 10,
    idleTimeoutMillis: 30000
}

var pool = new pg.Pool(config)
var myClient


module.exports = {
    addCaloriesEntry : function() {
        return new Promise(
            (resolve, reject) => {
                fireQuery("INSERT INTO calories (calories, entry_date, user_id) VALUES (1, 2, 70)",
                    (err, result) => {
                        if (err) {
                            console.log("Alex e prost")
                            reject(err)
                        }
                        console.log(result.rows[0])
                        resolve(result.rows[0])                        
                    },
                    (err) => {
                        console.log("Postgres e prost")
                        reject(err)
                    }
                )
            }
        )
    }
}

var fireQuery = function(query, afterTheQuery, postgresFailed) {
    pool.connect(function (err, client, done) {
        if (err) {
            console.log(err)
            //postgresFailed(err)
            //return;
        }
        myClient = client
        var caloriesQuery = format(query)
        myClient.query(caloriesQuery, afterTheQuery)
    })
}