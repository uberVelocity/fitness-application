var dbm = require("../model/databaseManager.js")

module.exports = {
    post: async function(req, res) {

        //var entry

        await validateEntry(req.body).then(
            () => {
                //this happens if the promise is resolved
                //here you create the entry
            },
            () => {
                //this happens if the promise is rejected
                res.status(403).send({error: "Your entry is not valid"})
            }
        )

        dbm.addCaloriesEntry().then(
            (row0) => {
                console.log("A fost un succes!")
                res.status(200).send(row0)
            },
            (error) => {
                console.log("Nu a fost un succes!")
                res.status(403).send(error)
            }
        )
    }
}

var validateEntry = async function(entry){
    return new Promise(
        (resolve, reject) => {
            //aici verifici daca datele sunt ce te astepti sa fie si dai resolve() daca sunt, sau reject() daca nu sunt
            resolve()
        }
    )
}