const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())

require("./router.js")(app)

app.listen(3000, function(){
    console.log("Server is listening to port 3000")
})