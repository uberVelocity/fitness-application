var pg = require('pg')
var format = require('pg-format')
var PGUSER = 'postgres'
var PGDATABASE = 'test'

var config = {
    user: PGUSER,
    database: PGDATABASE,
    max: 10,
    idleTimeoutMillis: 30000
}

var pool = new pg.Pool(config)
var myClient

module.exports = {
    getCalories : function(req, res) {
        pool.connect(function (err, client, done) {
            if (err) {
                console.log(err)
            }
            myClient = client
            var caloriesQuery = format('SELECT * FROM calories')
            myClient.query(caloriesQuery, function (err, result) {
                if (err) {
                    console.log(err)
                }
                console.log(result.rows[0])
                res.status(200).send(result.rows[0])
            })
        })
    }
}