var messageController = require("./controllers/messageController.js")
var caloriesController = require("./controllers/caloriesController.js")

module.exports = (app) => {
    app.post("/showMessage/:message", messageController.printMessage, messageController.testcuparams)
    app.post("/addCaloriesEntry", caloriesController.post)
}