# Fitness - Well-being fitness tracker

## Architecture
1. Clients make requests to services which are hosted on servers written in Node.js using Express.js.
2. Servers communicate with different resources to respond to the clients' requests. The servers are stateless, meaning that they **must not** know the context in which the client is in.
3. Servers may make requests to a database(s) which is currently a MongoDB db hosted on Azure.
4. The website of the application will be constructed using Vue.js.

## Serializing and Deserializing users through passport
In a typical web application, the credentials used to authenticate a user will only be transmitted during the login request. If authentication succeeds, a session will be established and maintained via a cookie set in the user's browser.

Each subsequent request will not contain credentials, but rather the unique cookie that identifies the session. In order to support login sessions, Passport will serialize and deserialize user instances to and from the session.

```js
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});
```

## Connecting to MongoDB through Node.js
```Node.js
const MongoClient = require(‘mongodb’).MongoClient;
const uri = "mongodb+srv://User:<password>@cluster0-jl8bq.azure.mongodb.net/test?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true });
client.connect(err => {
  const collection = client.db("test").collection("devices");
  // perform actions on the collection object
  client.close();
});
```
Replace `User` and `password` with MongoDB usernames and passwords. Since passwords have special characters they should be [URL encoded](https://docs.atlas.mongodb.com/troubleshoot-connection/#special-characters-in-connection-string-password).

## MongoDB Tutorial
A step-by-step tutorial on [how to use mongoDB locally](mongoDB-tut).

## Interesting references:
1. Data [privacy and security standards](https://www.cleverbridge.com/corporate/data-privacy-information-security/).
2. Field-level encryption, such as SSN, bank cards, health information etc. (https://searchcio.techtarget.com/definition/field-level-encryption).
3. Field-level encryption vs Transparent-database encryption (https://help.salesforce.com/articleView?id=mc_overview_faq_fle_vs_tde.htm&type=5).
4. Maven [tutorial](https://www.tutorialspoint.com/maven/).
5. Data Access Object - Design pattern that supports the principle of single isolation.
6. Storing [passwords in a database](https://www.vertabelo.com/blog/technical-articles/how-to-store-authentication-data-in-a-database-part-1).
7. Number 6 but with examples and nice description. [Storing secure passwords by hashing and salting](https://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/)
8. PostgresQL + Java tutorial (http://zetcode.com/java/postgresql/)

## Dependencies
1. MongoDB - Document-Oriented Database Program hosted w/ Azure [here](https://cloud.mongodb.com/v2/5d20fa4bff7a2504608874e0#clusters).
2. Expressjs - Node.js framework.
3. Vue.js - Web-development framework.
