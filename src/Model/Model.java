package Model;

import Model.User;

public class Model {

    private User user;

    public Model() {
        this.user = new User();
    }

    public Model(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
