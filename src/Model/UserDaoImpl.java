package Model;

import db.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDaoImpl implements UserDao {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    public UserDaoImpl() {}

    public void updateMeters(User user, int meters) {
        user.setMetersRun(meters);
        logger.log(Level.INFO, "updated meters...");
        String query = "UPDATE stats SET meters = " + meters + " WHERE id = 1;";
        System.out.println(query);
        logger.log(Level.INFO, "updated db meters...");
        try {
            Class.forName("org.postgresql.Driver");
            Properties props = new Properties();
            props.setProperty("user", "fred");
            props.setProperty("password", "secret");
            props.setProperty("ssl", "true");
            Connection conn = DriverManager.getConnection(Database.URL);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            rs.next();
            String result = rs.getString("meters");
            System.out.println("Retrieved result = " + result);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
