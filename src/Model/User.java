package Model;

public class User {

    private String name;
    private int metersRun;

    public User() {
        this.name = "Bob";
        this.metersRun = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMetersRun(int metersRun) {
        this.metersRun = metersRun;
    }



    public String getName() {
        return name;
    }

    public int getMetersRun() {
        return metersRun;
    }
}
