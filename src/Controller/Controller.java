package Controller;

import View.View;
import Model.Model;
import db.Database;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller {

    private Model model;
    private View view;
    private Database database;

    private Logger logger = Logger.getLogger(this.getClass().getName());

    public Controller(Model model, View view, Database database) {
        this.model = model;
        this.view = view;
        this.database = database;

        logger.log(Level.INFO, "* initialized controller...");

    }

    public Model getModel() {
        return model;
    }

    public View getView() {
        return view;
    }

    public Database getDatabase() {
        return database;
    }
}
