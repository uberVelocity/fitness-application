package db;

import java.util.logging.Logger;

public class Database {

    public final static String URL = "jdbc:postgresql://localhost/life";

    private Logger logger = Logger.getLogger(this.getClass().getName());

    public Database() {

    }

    public String getURL() {
        return URL;
    }
}
