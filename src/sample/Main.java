package sample;

import java.sql.*;
import java.util.Properties;

import View.View;
import Model.Model;
import Controller.Controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import Model.*;
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
        Class.forName("org.postgresql.Driver");
        String url = "jdbc:postgresql://localhost/test";
        Properties props = new Properties();
        props.setProperty("user", "fred");
        props.setProperty("password", "secret");
        props.setProperty("ssl", "true");
        String query = "SELECT * FROM something";
        Connection conn = DriverManager.getConnection(url);
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(query);
        rs.next();
        String result = rs.getString("id");
        System.out.println("Retrieved result = " + result);
        Model model = new Model();
        View view = new View();
        User user = new User();
        UserDaoImpl userImpl = new UserDaoImpl();
        userImpl.updateMeters(user, 30);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
