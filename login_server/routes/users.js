const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

// User model
const User = require('../models/User');


// Login Page
router.get('/login', (req, res) => res.render('login'));

// Register Page
router.get('/register', (req, res) => res.render('register'));

// Register Handle
router.post('/register', (req, res) => {
    // Shows all information from the form
    // console.log(req.body);
    const { name, email, password, password2 } = req.body;
    let errors = [];

    // Check required fields
    if (!name || !email || !password || !password2) {
        errors.push({ msg: 'Please fill in all fields'});
    }

    // Check passwords match
    if (password !== password2) {
        errors.push({ msg: 'Passwords do not match' });
    }

    // Check pass length
    if (password.length < 6) {
        errors.push({msg: 'Password should be at least 6 characters'});
    }

    if (errors.length > 0) {
        // Validation failed
        res.render('register', {  // You can pass objects in through templates since you want to display the errors + data
            errors,
            name,
            email,
            password,
            password2
        });
    }
    else {
        // Validation passed
        // We pass e-mail in the form and we want to match it to a user in db to see if they exist
        User.findOne({ email: email})   // Mongoose query that filters user by email => if it finds one, then email already in use
        .then(user => {
            if (user) {
                // User exists
                errors.push({ msg: 'Email is already registered'});
                res.render('register', {  // You can pass objects in through templates since you want to display the errors + data
                    errors,
                    name,
                    email,
                    password,
                    password2
                });
            }
            else {
                // Create new user with details given by web form
                const newUser = new User({
                    name: name,
                    email: email,
                    password: password  // This is plain text => need to encrypt => use bcrypt
                });

                // Hash password
                bcrypt.genSalt(10, (err, salt) => 
                 bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    // Set password to hashed
                    newUser.password = hash;
                    // Save user
                    newUser.save()
                    .then(user => {
                        req.flash('success_msg', 'You are now registered and can log in');  // Created flash message
                        res.redirect('/users/login');
                    })
                    .catch(err => console.log(err));

                }));

            }
        });  
    }
    
});

// Login Handle
router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/users/login',
        failureFlash: true
    })(req, res, next);
});

// Logout Handle
router.get('/logout', (req, res) => {
    req.logout();   // Given by passport
    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/login');
});

module.exports = router;